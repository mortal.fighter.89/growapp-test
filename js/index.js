'use strict';

function isIE() {
  var userAgent = navigator.userAgent;
  return userAgent.lastIndexOf("MSIE ") > -1 || userAgent.lastIndexOf("Trident/") > -1 || userAgent.lastIndexOf("Edge/") > -1;
}

function calcCredit() {
	var tarif = parseFloat($('#calc-raiting-percent').html().replace(',', '.'));
	
	var amount = parseInt($('#calc-amount').val());
	$('#calc-print-amount').html(amount);

	var days = parseInt($('#calc-days').val());
	$('#calc-print-days').html(days);

	var itogPercent = tarif * days;
	$('#calc-itog-percent').html((itogPercent + '%').replace('.', ','));

	var itogSum = amount + amount * itogPercent / 100;
	$('#calc-itog-sum').html((itogSum + '').replace('.', ','));
}

$(document).ready(function() {
	calcCredit();
	
	if (isIE()) {
		$('#calc-amount, #calc-days').on('change', calcCredit);	
	} else {
		$('#calc-amount, #calc-days').on('input', calcCredit);
	}
});
